--- 
title: "Una guia para R"
author: "Escuela de Datos - Fundación SOL"
date: "`r Sys.Date()`"
site: bookdown::bookdown_site
output: bookdown::gitbook
always_allow_html: true
documentclass: book
bibliography: [book.bib, packages.bib]
biblio-style: apalike
link-citations: yes
description: "Una breve guia para partir con R"
---

# ¿Qué debo saber antes?

En primer lugar es bueno saber que este texto y plataforma ha sido creada en R, utilizando un  _sample_ book elaborado en  **Markdown**.

Esto muestra una parte de la flexibilidad que tiene R como lenguaje de programación. 

A lo largo de esta guía iremos repasando los principales aspectos de R como: 

- Instalar R y Rstudio
- Introducción a R
- Estructuras de datos en R
- Funciones básicas y operadores
- Manipulación de datos con `dplyr`
- Visualización de datos con `ggplot2`

 
## Instalación de R

### Windows

Para instalar R en Windows, sigue estos pasos:

1. Ve a la página oficial de CRAN: [CRAN](https://cran.r-project.org/)
2. Haz clic en el enlace de descarga para Windows.
3. Descarga el instalador ejecutable de R.
4. Ejecuta el archivo descargado y sigue las instrucciones del asistente de instalación.

```{r, eval=FALSE}
# Enlace directo para descargar R para Windows ejecutando desde R
browseURL("https://cran.r-project.org/bin/windows/base/")
```

### macOS
```{r, eval=FALSE}
# Enlace directo para descargar R para Windows ejecutando desde R
browseURL("https://cran.r-project.org/bin/macosx/")
```

### RStudio para Windows
```{r, eval=FALSE}
# Enlace directo para descargar R para Windows ejecutando desde R
browseURL("https://www.rstudio.com/products/rstudio/download/#download")
```

## Algunas webs de utilidad

A continuación podrás ver algunos enlaces para profundizar tus conocimientos y autoaprendizaje en R: 


- [The Comprehensive R Archive Network (CRAN)](https://cran.r-project.org) - El repositorio oficial de paquetes de R.
- [RStudio](https://www.rstudio.com) - Un IDE popular para R.
- [R for Data Science](https://r4ds.had.co.nz) - Un libro gratuito en línea que cubre desde los fundamentos hasta técnicas avanzadas de R.
- [R-Bloggers](https://www.r-bloggers.com) - Un blog con artículos y tutoriales sobre R.
- [Swirl](https://swirlstats.com) - Un paquete de R que te permite aprender R interactivamente dentro de la consola de R.

### Mejores Páginas de YouTube para Aprender R

Aquí tienes una lista de algunos de los mejores canales de YouTube donde puedes aprender R:

1. [R para principiantes](https://cran.r-project.org/doc/contrib/rdebuts_es.pdf)
   - Texto de Emmanuel Paradis, traducido por Jorge A. Ahumada. (2002)
   
2. [The R Journal](https://journal.r-project.org/issues.html)
   - Revista en inglés orientada a publicaciones de uso de R en estadísticas y otros.

3. [freeCodeCamp.org](https://www.youtube.com/channel/UC8butISFwT-Wl7EV0hUK0BQ)
   - Ofrece un curso completo de R gratuito, desde los conceptos básicos hasta avanzados.

4. [Albert Rapp](https://www.youtube.com/@rappa753)
   - Canal de youtube que ofrece tutoriales sobre R y otros lenguajes de programación para ciencia de datos en inglés.


Explora estos canales para encontrar tutoriales, cursos y recursos que te ayudarán a aprender y mejorar tus habilidades en R.
